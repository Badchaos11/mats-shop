package repository

import (
	"context"

	"github.com/jackc/pgx/v5"
	"gitlab.com/Badchaos11/mats-shop/backend/model"
)

func (r *PGXRepository) CreateUser(ctx context.Context, user model.NewUser) error {
	const query = `INSERT INTO users (username, password, email, role, created, updated) VALUES ($1, $2, $3, $4, now(), now());`
	ctx, cancel := context.WithTimeout(ctx, r.timeout)
	defer cancel()

	_, err := r.MasterConn.Exec(ctx, query, user.Username, user.Password, user.Email, 1)
	if err != nil {
		return err
	}

	return nil
}

func (r *PGXRepository) CheckUserExistance(ctx context.Context, username string) (bool, error) {
	const query = `SELECT username FROM users WHERE username=$1`
	ctx, cancel := context.WithTimeout(ctx, r.timeout)
	defer cancel()
	var u string

	err := r.MasterConn.QueryRow(ctx, query, username).Scan(&u)
	if err != nil {
		if err == pgx.ErrNoRows {
			return true, nil
		}
		return true, err
	}

	return false, nil
}

func (r *PGXRepository) LoginUser(ctx context.Context, u model.User) (*model.User, error) {
	const query = `SELECT username, email, role FROM users WHERE username=$1 AND password=$2`
	ctx, cancel := context.WithTimeout(ctx, r.timeout)
	defer cancel()
	var out model.User

	err := r.MasterConn.QueryRow(ctx, query, u.Username, u.Password).Scan(&out.Username, &out.Email, &out.Role)
	if err != nil {
		if err == pgx.ErrNoRows {
			return nil, nil
		}
		return nil, err
	}
	return &out, nil
}

func (r *PGXRepository) ChangePassword(ctx context.Context, u model.User) error {
	const query = `UPDATE users SET password=$2, updated=now() WHERE username=$1;`
	ctx, cancel := context.WithTimeout(ctx, r.timeout)
	defer cancel()

	_, err := r.MasterConn.Exec(ctx, query, u.Username, u.Password)
	if err != nil {
		return err
	}

	return nil
}

func (r *PGXRepository) GetSelfUser(ctx context.Context, username string) (*model.User, error) {
	const query = `SELECT username, email, role, created FROM users WHERE username=$1`
	ctx, cancel := context.WithTimeout(ctx, r.timeout)
	defer cancel()
	var u model.User

	err := r.MasterConn.QueryRow(ctx, query, username).Scan(&u.Username, &u.Email, &u.Role, &u.Created)
	if err != nil {
		if err == pgx.ErrNoRows {
			return nil, nil
		}
		return nil, err
	}
	return &u, nil
}

func (r *PGXRepository) DeleteUser(ctx context.Context, userId int64) error {
	const query = `UPDATE users SET deleted=now() WHERE id=$1`
	ctx, cancel := context.WithTimeout(ctx, r.timeout)
	defer cancel()

	_, err := r.MasterConn.Exec(ctx, query, userId)
	if err != nil {
		return err
	}
	return nil
}

func (r *PGXRepository) ChangeUserRole(ctx context.Context, u model.User) (bool, error)

func (r *PGXRepository) GetUserInfoByID(ctx context.Context, userId int64) (*model.User, error)
