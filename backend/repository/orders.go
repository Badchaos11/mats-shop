package repository

import (
	"context"

	"gitlab.com/Badchaos11/mats-shop/backend/model"
)

const initialStatus = "в обработке"

func (r *PGXRepository) CreateOrder(ctx context.Context, o model.Order) (int64, error) {
	const query = `INSERT INTO orders (goods_id, buyer_id, price, status, created, updated) 
				   VALUES ($1, $2, $3, $4, now(), now()) RETURNING id`
	ctx, cancel := context.WithTimeout(ctx, r.timeout)
	defer cancel()
	var id int64

	err := r.MasterConn.QueryRow(ctx, query, o.GoodID, o.BuyerID, o.Price, initialStatus).Scan(&id)
	if err != nil {
		return 0, err
	}

	return id, err
}
