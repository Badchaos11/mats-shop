package kvRepo

import "context"

func (c *kvRepo) AddToCache(ctx context.Context, key, value string) error

func (c *kvRepo) GetFromCache(ctx context.Context, key string) (string, error)

func (c *kvRepo) ClearCache(ctx context.Context) error
