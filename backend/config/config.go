package config

import (
	"os"

	"github.com/joho/godotenv"
	"github.com/sirupsen/logrus"
	"gitlab.com/Badchaos11/mats-shop/backend/model"
)

func LoadConfig() (*model.Config, error) {

	err := godotenv.Load("./configs/.env")
	if err != nil {
		logrus.Errorf("failed to load config: %v", err)
		return nil, err
	}

	return &model.Config{
		Port:       os.Getenv("PORT"),
		DbHost:     os.Getenv("DB_HOST"),
		DbUser:     os.Getenv("DB_USER"),
		DbName:     os.Getenv("DB_NAME"),
		DbPassword: os.Getenv("DB_PASSWORD"),
		CacheUrl:   os.Getenv("CACHE_URL"),
	}, nil
}
