package service

import (
	"context"
	"fmt"
	"net/http"
	"os"
	"os/signal"
	"time"

	"github.com/gorilla/mux"
	"github.com/sirupsen/logrus"
	"gitlab.com/Badchaos11/mats-shop/backend/model"
	"gitlab.com/Badchaos11/mats-shop/backend/repository"
)

type service struct {
	port string
	repo repository.IRepository
}

func NewService(ctx context.Context, conf model.Config) *service {
	dsn := fmt.Sprintf("host=%s user=%s password=%s dbname=%s sslmode=disable", conf.DbHost, conf.DbUser, conf.DbPassword, conf.DbName)
	repo, err := repository.NewRepository(ctx, dsn, conf.CacheUrl)
	if err != nil {
		panic(err)
	}
	return &service{
		repo: repo,
	}
}

func (s *service) Run() {
	router := mux.NewRouter()

	public := router.NewRoute().Subrouter()
	owner := router.NewRoute().Subrouter()
	user := router.NewRoute().Subrouter()

	public.HandleFunc("/api/register", s.Register).Methods("POST")
	public.HandleFunc("/api/login", s.Login).Methods("POST")
	public.HandleFunc("/api/goods", s.LoadGoodsForShow).Methods("GET")
	public.HandleFunc("/api/filtered-goods", s.GetFilteredGoodsForShow).Methods("GET")
	public.HandleFunc("/api/detailed-goods", s.GetDetailedGoods).Methods("GET")

	owner.HandleFunc("/api/owner/goods/create", s.CreateGoods).Methods("POST")
	owner.HandleFunc("/api/owner/goods/update", s.UpdateGoods).Methods("POST")
	owner.HandleFunc("/api/owner/goods/add-images", s.AddImagesToGoods).Methods("POST")
	owner.HandleFunc("/api/owner/goods/add-video", s.AddVideoToGoods).Methods("POST")
	owner.HandleFunc("/api/owner/goods/all", s.GetAllMyGoods).Methods("GET")
	owner.HandleFunc("/api/owner/goods/delete", s.DeleteGoods).Methods("DELETE")
	owner.HandleFunc("/api/owner/orders/update", s.UpdateOrderStatus).Methods("POST")
	owner.HandleFunc("/api/owner/orders/delete", s.DeleteOrder).Methods("DELETE")
	owner.HandleFunc("/api/owner/orders/all", s.GetAllOwnerOrders).Methods("GET")
	owner.HandleFunc("/api/owner/send-message", s.SendMessageToBuyer).Methods("POST")

	user.HandleFunc("/api/user/change-password", s.ChangePassword).Methods("POST")
	user.HandleFunc("/api/user/orders/create", s.CreateOrder).Methods("POST")
	user.HandleFunc("api/user/orders/get-one", s.GetOrderByID).Methods("GET")
	user.HandleFunc("/api/user/orders/all", s.GetAllMyOrders).Methods("GET")
	user.HandleFunc("/api/user/orders/reject", s.RejectOrder).Methods("POST")

	server := &http.Server{
		Addr:         fmt.Sprintf(":%s", s.port), // Порт сервера
		Handler:      router,                     // Хэндлеры
		ReadTimeout:  5 * time.Second,            // Таймаут запроса клиента
		WriteTimeout: 10 * time.Second,           // Таймаут ответа клиенту
		IdleTimeout:  120 * time.Second,          // Таймаут соединения в простое
	}

	go func() {
		logrus.Infof("starting server on port %v", s.port)

		err := server.ListenAndServe()
		if err != nil {
			logrus.Errorf("error starting server %v", err)
			os.Exit(1)
		}
	}()

	// Отключение
	c := make(chan os.Signal, 1)
	signal.Notify(c, os.Interrupt)

	sig := <-c
	logrus.Infof("Got signal: %v", sig)

	ctx, cancel := context.WithTimeout(context.Background(), 30*time.Second)
	defer cancel()

	server.Shutdown(ctx)
}

func init() {

	logrus.SetFormatter(&logrus.TextFormatter{
		TimestampFormat: "2006-01-02 15:04:05",
		ForceColors:     true,
		DisableColors:   false,
		FullTimestamp:   true,
	})
	logrus.SetLevel(logrus.DebugLevel)
}
