package service

import (
	"context"
	"encoding/json"
	"fmt"
	"net/http"

	"github.com/sirupsen/logrus"
	"gitlab.com/Badchaos11/mats-shop/backend/model"
)

func (s *service) CreateOrder(w http.ResponseWriter, r *http.Request) {
	var req model.NewUser

	err := s.ParseToModel(r, req)
	if err != nil {
		logrus.Errorf("error parsing request body %v", err)
		s.WriteResponse(w, http.StatusInternalServerError, "Не удалось зарегистрировать нового пользователя")
		return
	}

	if req.Password != req.PasswordConfirmation {
		logrus.Error("passwords must be equal")
		s.WriteResponse(w, http.StatusBadRequest, "Пароли должнны совпадать")
		return
	}

	ctx := context.Background()
	exists, err := s.repo.CheckUserExistance(ctx, req.Username)
	if err != nil {
		logrus.Errorf("error checking user existance %v", err)
		s.WriteResponse(w, http.StatusInternalServerError, "Не удалось создать нового пользователя")
		return
	}
	if exists {
		logrus.Error("username already exists")
		s.WriteResponse(w, http.StatusBadRequest, "Такой пользователь уже существует")
		return
	}

	err = s.repo.CreateUser(ctx, req)
	if err != nil {
		logrus.Errorf("error creating user %v", err)
		s.WriteResponse(w, http.StatusInternalServerError, "Не удалось создать пользователя")
		return
	}

	s.WriteResponse(w, http.StatusOK, fmt.Sprintf("Пользователь успешно создан"))
}

func (s *service) GetOrderByID(w http.ResponseWriter, r *http.Request)

func (s *service) GetAllMyOrders(w http.ResponseWriter, r *http.Request)

func (s *service) ChangePassword(w http.ResponseWriter, r *http.Request) {
	var req model.User

	if err := s.ParseToModel(r, &req); err != nil {
		logrus.Errorf("error parsing request body %v", err)
		s.WriteResponse(w, http.StatusInternalServerError, "Не удалось изменить пароль")
		return
	}

	username := r.Context().Value(0)
	ctx := context.Background()

	err := s.repo.ChangePassword(ctx, model.User{Username: username.(string), Password: req.Password})
	if err != nil {
		logrus.Errorf("error changing password %v", err)
		s.WriteResponse(w, http.StatusInternalServerError, "Не удалось изменить пароль")
		return
	}

	s.WriteResponse(w, http.StatusOK, "Пароль успешно изменен")
}

func (s *service) RejectOrder(w http.ResponseWriter, r *http.Request)

func (s *service) GetSelfUser(w http.ResponseWriter, r *http.Request) {
	username := r.Context().Value(0)
	if username == "" {
		logrus.Error("error reading username from context")
		s.WriteResponse(w, http.StatusInternalServerError, "Не удалось получить данные пользователя")
		return
	}

	ctx := context.Background()
	user, err := s.repo.GetSelfUser(ctx, username.(string))
	if err != nil {
		logrus.Errorf("error getting user from db %v", err)
		s.WriteResponse(w, http.StatusInternalServerError, "Не удалост получить данные пользователя")
		return
	}

	body, err := json.Marshal(user)
	if err != nil {
		logrus.Errorf("error marshalling user %v", err)
		s.WriteResponse(w, http.StatusInternalServerError, "Не удалось получить данные пользователя")
		return
	}

	w.WriteHeader(http.StatusOK)
	w.Write([]byte(body))
}
