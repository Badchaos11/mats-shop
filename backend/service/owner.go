package service

import "net/http"

func (s *service) CreateGoods(w http.ResponseWriter, r *http.Request)

func (s *service) UpdateGoods(w http.ResponseWriter, r *http.Request)

func (s *service) AddImagesToGoods(w http.ResponseWriter, r *http.Request)

func (s *service) AddVideoToGoods(w http.ResponseWriter, r *http.Request)

func (s *service) DeleteGoods(w http.ResponseWriter, r *http.Request)

func (s *service) UpdateOrderStatus(w http.ResponseWriter, r *http.Request)

func (s *service) DeleteOrder(w http.ResponseWriter, r *http.Request)

func (s *service) SendMessageToBuyer(w http.ResponseWriter, r *http.Request)

func (s *service) GetAllMyGoods(w http.ResponseWriter, r *http.Request)

func (s *service) GetAllOwnerOrders(w http.ResponseWriter, r *http.Request)
