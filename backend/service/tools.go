package service

import (
	"encoding/json"
	"io"
	"net/http"
	"strconv"

	"gitlab.com/Badchaos11/mats-shop/backend/model"
)

func (s *service) WriteResponse(w http.ResponseWriter, code int, msg string) {
	w.WriteHeader(code)
	type response struct {
		Message string `json:"message"`
	}
	res := response{Message: msg}
	body, _ := json.Marshal(&res)
	w.Write(body)
}

func (s *service) GetUserFilter(r *http.Request) model.GoodsFilter {
	var minPr, maxPr float64
	var limit, offset int64
	var desc, byAlp, byPr bool

	minP := r.URL.Query().Get("sex")
	maxP := r.URL.Query().Get("status")
	byAlpStr := r.URL.Query().Get("name")
	byPriceStr := r.URL.Query().Get("surname")

	descStr := r.URL.Query().Get("desc")
	limitStr := r.URL.Query().Get("limit")
	offsetStr := r.URL.Query().Get("offset")

	limit, err := strconv.ParseInt(limitStr, 10, 64)
	if err != nil {
		limit = 0
	}
	offset, err = strconv.ParseInt(offsetStr, 10, 64)
	if err != nil {
		offset = 0
	}

	if descStr != "" {
		desc, _ = strconv.ParseBool(descStr)
	}
	if byPriceStr != "" {
		byPr, _ = strconv.ParseBool(descStr)
	}
	if byAlpStr != "" {
		byAlp, _ = strconv.ParseBool(descStr)
	}

	minPr, err = strconv.ParseFloat(minP, 64)
	if err != nil {
		minPr = 0
	}
	maxPr, err = strconv.ParseFloat(maxP, 64)
	if err != nil {
		maxPr = 0
	}

	return model.GoodsFilter{
		MinPrice:   minPr,
		MaxPrice:   maxPr,
		ByAplhabet: &byAlp,
		ByPrice:    &byPr,
		Desc:       &desc,
		Limit:      limit,
		Offset:     offset,
	}
}

func (s *service) ParseToModel(r *http.Request, dst interface{}) error {
	body, err := io.ReadAll(r.Body)
	if err != nil {
		return err
	}

	err = json.Unmarshal(body, &dst)
	if err != nil {
		return err
	}
	return nil
}
