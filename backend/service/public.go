package service

import (
	"context"
	"net/http"
	"time"

	"github.com/sirupsen/logrus"
	"gitlab.com/Badchaos11/mats-shop/backend/middleware"
	"gitlab.com/Badchaos11/mats-shop/backend/model"
)

func (s *service) Register(w http.ResponseWriter, r *http.Request) {
	var req model.NewUser

	if err := s.ParseToModel(r, &req); err != nil {
		logrus.Errorf("error parsing request body %v", err)
		s.WriteResponse(w, http.StatusInternalServerError, "Не удалось создать пользователя")
		return
	}
	ctx := context.Background()
	exists, err := s.repo.CheckUserExistance(ctx, req.Username)
	if err != nil {
		logrus.Errorf("error getting user from db %v", err)
		s.WriteResponse(w, http.StatusInternalServerError, "Не удалось создать пользователя")
		return
	}
	if exists {
		logrus.Errorf("username already exists")
		s.WriteResponse(w, http.StatusBadRequest, "Пользователь с таким логином уже существует")
		return
	}
	if req.Password != req.PasswordConfirmation {
		logrus.Error("passwords must be equal")
		s.WriteResponse(w, http.StatusBadRequest, "Пароли должны совпадать")
		return
	}

	err = s.repo.CreateUser(ctx, req)
	if err != nil {
		logrus.Errorf("error creating user in db %v", err)
		s.WriteResponse(w, http.StatusInternalServerError, "Не удалось создать пользователя")
		return
	}

	s.WriteResponse(w, http.StatusOK, "Пользователь успешно создан, перенаправление на страницу авторизации")
}

func (s *service) Login(w http.ResponseWriter, r *http.Request) {
	var req model.User

	if err := s.ParseToModel(r, &req); err != nil {
		logrus.Errorf("error parsing request body %v", err)
		s.WriteResponse(w, http.StatusInternalServerError, "Не удалось создать пользователя")
		return
	}
	ctx := context.Background()
	user, err := s.repo.LoginUser(ctx, req)
	if err != nil {
		logrus.Errorf("error getting user from db %v", err)
		s.WriteResponse(w, http.StatusInternalServerError, "Не удалось войти в аккаунт")
		return
	}
	if user == nil {
		logrus.Error("user not found")
		s.WriteResponse(w, http.StatusNotFound, "Пользователь не найден, проверьте введенные данные")
		return
	}

	token, err := middleware.NewToken(ctx, user.Username, int32(user.Role))
	if err != nil {
		logrus.Errorf("error creating access token %v", err)
		s.WriteResponse(w, http.StatusInternalServerError, "Не удалось войти в аккаунт")
		return
	}

	http.SetCookie(w, &http.Cookie{
		Name:    "token",
		Value:   token,
		Expires: time.Now().Add(time.Hour * 72),
	})

	s.WriteResponse(w, http.StatusOK, "Выполнен вход в аккаунт")
}

func (s *service) Logout(w http.ResponseWriter, r *http.Request) {
	сookie := &http.Cookie{
		Name:   "token",
		MaxAge: -1,
	}
	http.SetCookie(w, сookie)

	s.WriteResponse(w, http.StatusOK, "Выполнен выход из аккаунта")
}

func (s *service) LoadGoodsForShow(w http.ResponseWriter, r *http.Request)

func (s *service) GetFilteredGoodsForShow(w http.ResponseWriter, r *http.Request)

func (s *service) GetDetailedGoods(w http.ResponseWriter, r *http.Request)

func (s *service) WriteToSeller(w http.ResponseWriter, r *http.Request)
