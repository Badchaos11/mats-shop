package main

import (
	"context"

	"gitlab.com/Badchaos11/mats-shop/backend/config"
	"gitlab.com/Badchaos11/mats-shop/backend/service"
)

func main() {
	conf, err := config.LoadConfig()
	if err != nil {
		panic(err)
	}
	ctx := context.Background()
	app := service.NewService(ctx, *conf)

	app.Run()
}
