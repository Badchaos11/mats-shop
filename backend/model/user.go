package model

import "time"

type UserRole int32

const (
	AdminUser  UserRole = iota + 1 // админ
	OwnerUser                      // владелец товара, продавец
	ModerUser                      // модератор если вдруг понадобится
	CommonUser                     // обычный покупатель
)

type NewUser struct {
	Username             string `json:"username"`
	Password             string `json:"password"`
	PasswordConfirmation string `json:"password_confirmation,omitempty"`
	Email                string `json:"email,omitempty"`
}

type User struct {
	ID       int64      `json:"id,omitempty"`
	Username string     `json:"username,omitempty"`
	Password string     `json:"password,omitempty"`
	Email    string     `json:"email,omitempty"`
	Role     UserRole   `json:"role,omitempty"`
	Created  time.Time  `pg:"created,notnull" json:"created,omitempty"`
	Updated  time.Time  `pg:"updated,notnull" json:"updated,omitempty"`
	Deleted  *time.Time `json:"deleted,omitempty"`
}
