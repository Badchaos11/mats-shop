package model

type Config struct {
	Port       string
	DbHost     string
	DbUser     string
	DbPassword string
	DbName     string
	CacheUrl   string
}
