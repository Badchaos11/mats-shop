package model

import "time"

type Order struct {
	ID      int64      `json:"id,omitempty"`
	GoodID  int64      `json:"good_id"`
	BuyerID int64      `json:"user_id"`
	Price   float32    `json:"price"`
	Status  string     `json:"status,omitempty"`
	Created time.Time  `pg:"created,notnull" json:"created"`
	Updated time.Time  `pg:"updated,notnull" json:"updated"`
	Deleted *time.Time `json:"deleted,omitempty"`
}

type OrderCard struct {
	GoodID  int64     `json:"good_id"`
	Price   float32   `json:"price"`
	Status  string    `json:"status,omitempty"`
	Updated time.Time `pg:"updated,notnull" json:"updated"`
}
