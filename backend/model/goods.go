package model

import "time"

type Goods struct {
	Id          int64      `json:"id" pg:"id,pk"`
	GoodName    string     `json:"good_name" pg:"good_name,notnull"`
	OwnerID     int64      `json:"user_id" pg:"user_id,notnull"`
	Price       float64    `json:"price" pg:"price,notnull"`
	Category    string     `json:"category" pg:"category"`
	SubCategory string     `json:"sub_category" pg:"sub_category"`
	Description string     `json:"description" pg:"description"`
	Image       string     `json:"image" pg:"image"`
	GoodID      int64      `json:"good_id" pg:"good_id"`
	Rating      float32    `json:"rate"`
	Created     time.Time  `pg:"created,notnull" json:"created"`
	Updated     time.Time  `pg:"updated,notnull" json:"updated"`
	Deleted     *time.Time `json:"deleted,omitempty"`
}

type GoodsFilter struct {
	MinPrice   float64  `json:"min_price,omitempty"`
	MaxPrice   float64  `json:"max_price,omitempty"`
	ByAplhabet *bool    `json:"by_alphabet,omitempty"`
	ByPrice    *bool    `json:"by_price,omitempty"`
	Desc       *bool    `json:"desc,omitempty"`
	Categories []string `json:"categories,omitempty"`
	Limit      int64    `json:"limit,omitempty"`
	Offset     int64    `json:"offset,omitempty"`
}

type GoodsCard struct {
	Name      string  `json:"name"`
	ImgPath   string  `json:"img_path"`
	VideoPath string  `json:"video_path"`
	Price     float64 `json:"price"`
	Rating    float64 `json:"rating"`
}
