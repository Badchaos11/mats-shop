package nats

import "context"

type INats interface {
	Send(ctx context.Context) error
}
