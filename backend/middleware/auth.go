package middleware

import (
	"context"
	"net/http"

	"github.com/sirupsen/logrus"
	"gitlab.com/Badchaos11/mats-shop/backend/model"
)

type key int

const (
	keyUsername key = iota
)

func AuthMiddleware(next http.Handler) http.Handler {
	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		tokenCookie, err := r.Cookie("token")
		if err != nil {
			logrus.Errorf("error getting token: %v", err)
			w.WriteHeader(http.StatusUnauthorized)
			w.Write([]byte("Отсутствует токе авторизации. Пожалуйста, войдите в аккаунт"))
			return
		}
		if tokenCookie == nil {
			logrus.Errorf("token cookie is nil")
			w.WriteHeader(http.StatusUnauthorized)
			w.Write([]byte("Сначала выполните вход в аккаунт."))
			return
		}
		tokenStr := tokenCookie.Value

		authJwt, err := parseToken(tokenStr, jwtSecretSignature)
		if err != nil {
			logrus.Errorf("error parsing jwt token error: %v", err)
			w.WriteHeader(http.StatusUnauthorized)
			w.Write([]byte("Выполните вход в аккаунт"))
			return
		}

		if authJwt == nil {
			logrus.Errorf("token is nil")
			w.WriteHeader(http.StatusUnauthorized)
			w.Write([]byte("Выполните вход в аккаунт."))
			return
		}
		ctx := r.Context()
		ctx = context.WithValue(ctx, keyUsername, authJwt.Username)
		r = r.WithContext(ctx)
		next.ServeHTTP(w, r)
	})
}

func AuthOwnerMiddleware(next http.Handler) http.Handler {
	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		tokenCookie, err := r.Cookie("token")
		if err != nil {
			logrus.Errorf("error getting token: %v", err)
			w.WriteHeader(http.StatusUnauthorized)
			w.Write([]byte("Отсутствует токе авторизации. Пожалуйста, войдите в аккаунт"))
			return
		}
		if tokenCookie == nil {
			logrus.Errorf("token cookie is nil")
			w.WriteHeader(http.StatusUnauthorized)
			w.Write([]byte("Сначала выполните вход в аккаунт."))
			return
		}
		tokenStr := tokenCookie.Value

		authJwt, err := parseToken(tokenStr, jwtSecretSignature)
		if err != nil {
			logrus.Errorf("error parsing jwt token error: %v", err)
			w.WriteHeader(http.StatusUnauthorized)
			w.Write([]byte("Выполните вход в аккаунт"))
			return
		}

		if authJwt == nil {
			logrus.Errorf("token is nil")
			w.WriteHeader(http.StatusUnauthorized)
			w.Write([]byte("Выполните вход в аккаунт."))
			return
		}

		if authJwt.UserRole != int32(model.OwnerUser) && authJwt.UserRole != int32(model.AdminUser) {
			logrus.Error("access denied")
			w.WriteHeader(http.StatusForbidden)
			w.Write([]byte("Недостаточно прав для совершения данного действия"))
			return
		}
		ctx := r.Context()
		ctx = context.WithValue(ctx, keyUsername, authJwt.Username)
		r = r.WithContext(ctx)
		next.ServeHTTP(w, r)
	})
}
